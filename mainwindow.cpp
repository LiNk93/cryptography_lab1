#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tableWidget->setRowCount(5);
    ui->tableWidget->setColumnCount(5);
    ui->tableWidget_2->setRowCount(5);
    ui->tableWidget_2->setColumnCount(5);
    message.setWindowTitle(trUtf8("Внимание!"));
    alphabet = trUtf8("АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ");
    message.setModal(true);
    for(int i=0; i<5; i++)
        for(int j=0; j<5; j++)
        {
            ui->tableWidget->setItem(i, j, new QTableWidgetItem(' '));
            ui->tableWidget_2->setItem(i, j, new QTableWidgetItem(' '));
        }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    cryptstr.clear();
    QSet<QChar> alph;
    foreach(const QChar c, alphabet)
        alph << c;

    mystr = ui->lineEdit->text();
    len = mystr.length();
    if (len == 0)
    {
        message.showMessage(trUtf8("Введите ключ!"));
        return;
    }
    if ((len % 2) == 1)
    {
        mystr.append(' ');
        ui->lineEdit->setText(mystr);
        len++;
    }

    QSet<QChar> myset;
    foreach(const QChar c, mystr)
        myset << c;

    QList<QChar> bb = (alph - myset).toList();
    std::random_shuffle(bb.begin(), bb.end());
    QList<QChar> table1 = myset.toList() + bb.mid(0, 5 * 5 - myset.size());
    std::random_shuffle(bb.begin(), bb.end());
    QList<QChar> table2 = myset.toList() + bb.mid(0, 5 * 5 - myset.size());
    std::random_shuffle(table1.begin(), table1.end());
    std::random_shuffle(table2.begin(), table2.end());

    for(int i=0; i<5; i++)
        for(int j=0; j<5; j++)
        {
            ui->tableWidget->setItem(i, j, new QTableWidgetItem(table1.at(i * 5 + j)));
            ui->tableWidget_2->setItem(i, j, new QTableWidgetItem(table2.at(i * 5 + j)));
        }

    int x, c, y, g;

    for (int s = 0; s < len; s++) // Поиск квадрата
    {
        if((s % 2) == 0)
        {
            for(int i=0; i<5; i++)
                for(int j=0; j<5; j++)
                {
                    if ( QString(mystr.at(s)) == ui->tableWidget->item(i, j)->text() )
                    {
                        x = i;
                        c = j;
                    }
                }
        }
        else
        {
            for(int i=0; i<5; i++)
                for(int j=0; j<5; j++)
                {
                    if ( QString(mystr.at(s)) == ui->tableWidget_2->item(i, j)->text())
                    {
                        y = i;
                        g = j;
                    }
                }
            // Вычисление квадрата
            if (s>0)
            {
                if (x == y)
                {
                    a = ui->tableWidget->item(y, g)->text();
                    b = ui->tableWidget_2->item(x, c)->text();
                }
                else
                {
                    a = ui->tableWidget->item(y, c)->text();
                    b = ui->tableWidget_2->item(x, g)->text();
                }
                cryptstr.append(a);
                cryptstr.append(b);
            }
        }
    }

    ui->lineEdit_2->setText( cryptstr );

}

void MainWindow::on_pushButton_2_clicked() // Дешифровка
{
    int x, c, y, g;
    cryptstr = ui->lineEdit_2->text();
    len = cryptstr.length();
    newstr.clear();
    for(int s = 0; s < len; s++)// Поиск квадрата
    {
        if((s % 2) == 0)
        {
            for(int i=0; i<5; i++)
                for(int j=0; j<5; j++)
                {
                    if ( QString(cryptstr.at(s)) == ui->tableWidget->item(i, j)->text())
                    {
                        x = i;
                        c = j;
                    }
                }

        }
        else
        {
            for(int i=0; i<5; i++)
                for(int j=0; j<5; j++)
                {
                    if ( QString(cryptstr.at(s)) == ui->tableWidget_2->item(i, j)->text())
                    {
                        y = i;
                        g = j;
                    }
                }
            // Вычисление квадрата
            if (s > 0)
            {
                if (x == y)
                {
                    a = ui->tableWidget->item(y, g)->text();
                    b = ui->tableWidget_2->item(x, c)->text();
                }
                else
                {
                    a = ui->tableWidget->item(y, c)->text();
                    b = ui->tableWidget_2->item(x, g)->text();
                }
                newstr.append(a);
                newstr.append(b);
            }
        }
    }

    ui->lineEdit_3->setText(newstr);

    if (mystr == newstr)
    {
        message.showMessage(trUtf8("Результаты совпали"));
        return;
    }
}

void MainWindow::on_pushButton_3_clicked() // Запись в файл
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), "",
    tr("Text Files (*.txt);;C++ Files (*.cpp *.h)"));
    if (fileName != "") {
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly)) {
            // error message
        } else {
            QTextStream stream(&file);
            for(int i=0; i<5; i++)
            {
                for(int j=0; j<5; j++){
                    stream << ui->tableWidget->item(i, j)->text();
                    stream.flush();
                }
                stream << "\n";
                stream.flush();
            }
            stream << "\n"; // Разделитель между таблицами
            stream.flush();
            for(int i=0; i<5; i++)
            {
                for(int j=0; j<5; j++){
                    stream << ui->tableWidget_2->item(i, j)->text();
                    stream.flush();
                }
                stream << "\n";
                stream.flush();
            }
            file.close();
        }
    }
}

void MainWindow::on_pushButton_4_clicked() // Чтение из файла
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "",
    tr("Text Files (*.txt);;C++ Files (*.cpp *.h)"));
    if (fileName != "") {
        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly)) {
            message.showMessage("Could not open file");
            return;
        }
        QTextStream in(&file);
        for(int i=0; i<5; i++)
        {
            for(int j=0; j<5; j++){
                ui->tableWidget->item(i, j)->setText(in.read(1));
            }
            in.read(1);
        }
        in.read(1);
        for(int i=0; i<5; i++)
        {
            for(int j=0; j<5; j++){
                ui->tableWidget_2->item(i, j)->setText(in.read(1));
            }
            in.read(1);
        }
        file.close();
    }
}
